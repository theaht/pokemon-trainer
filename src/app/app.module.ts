import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {LandingContainer} from './features/landing/containers/landing.container';
import {PokemonCatalogueContainer} from './features/pokemon-catalogue/containers/pokemon-catalogue.container';
import {PokemonDetailContainer} from './features/pokemon-detail/containers/pokemon-detail.container';
import {TrainerContainer} from './features/trainer/containers/trainer.container';
import {ReactiveFormsModule} from '@angular/forms';
import {PokemonCatalogueService} from './features/pokemon-catalogue/services/pokemon-catalogue.service';
import {HttpClientModule} from '@angular/common/http';
import {NavbarComponent} from './shared/navbar/navbar.component';

@NgModule({
  declarations: [
    AppComponent,
    LandingContainer,
    PokemonCatalogueContainer,
    PokemonDetailContainer,
    TrainerContainer,
    NavbarComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
