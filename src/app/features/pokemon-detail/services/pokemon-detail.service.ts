
import {Injectable} from '@angular/core';
import {Pokemon} from '../../../models/pokemon.model';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../environments/environment';
import {map} from 'rxjs/operators';

const { pokeAPI } = environment;

@Injectable({
  providedIn: 'root'
})
export class PokemonDetailService{
  public pokemon: Pokemon;

  /**
   * Constructor. initialize a httpclient
   */
  constructor(private readonly http: HttpClient) {
  }
  /**
   * Fetches the information about the given pokemon from the API.
   */
  public fetchPokemonByName(name: string): void {
    this.http.get<Pokemon>(`${pokeAPI}/${name}`)
      .pipe(
        map((pokemon: Pokemon) => ({
          ...pokemon,
          image: `https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/${ pokemon.id }.png`
        }))
      )
    .subscribe((pokemon: Pokemon) => {
      this.pokemon = pokemon;
    });
  }
  /**
   * Returns the name, id and image of the given pokemon
   */
  getIdAndImage(id: string, name: string): Pokemon {
    return {name, id, image: `https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/${ id }.png`};
  }
}
