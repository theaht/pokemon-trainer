import {Component, OnInit} from '@angular/core';
import {PokemonDetailService} from '../services/pokemon-detail.service';
import {ActivatedRoute} from '@angular/router';
import {Pokemon} from '../../../models/pokemon.model';
import {getStorage, setStorage} from '../../../utils/storage.utils';
import {Trainer} from '../../../models/trainer.model';
import {StorageKeys} from '../../../enums/storage-keys.enum';

@Component({
  selector: 'app-pokemon-detail-container',
  templateUrl: './pokemon-detail.container.html',
  styleUrls: ['./pokemon-detail.container.css']
})
export class PokemonDetailContainer implements OnInit {

  private readonly pokemonName: string = '';
  private storage: Trainer = {name: '', pokemons: []};

  /**
   * Constructor.
   * Set the pokemonName from the parameter in the route url.
   */
  constructor(
    private readonly pokemonDetailService: PokemonDetailService,
    private readonly route: ActivatedRoute ){
      this.pokemonName = this.route.snapshot.paramMap.get('name');
  }

  /**
   * Returns the Pokemon-object from the pokemonDetailService
   */
  get pokemon(): Pokemon {
    return this.pokemonDetailService.pokemon;
  }

  /**
   * On initialization, call the fetch function in the service,
   * to get information on the given pokemon from the API.
   */
  ngOnInit(): void {
    this.pokemonDetailService.fetchPokemonByName(this.pokemonName);

  }
  /**
   * This function is called when the user clicks the collect button.
   * It creates a pokemon object containing the name, id and image of the pokemon and
   * adds this object to the list of pokemons in localstorage.
   */
  collectPokemon(): void {
    this.storage = getStorage(StorageKeys.TRAINER);
    const storedPokemon: Pokemon = this.pokemonDetailService.getIdAndImage(this.pokemon.id, this.pokemon.name);
    setStorage(StorageKeys.TRAINER, {
      name: this.storage.name,
      pokemons: [...this.storage.pokemons, storedPokemon]
      });
  }
}

