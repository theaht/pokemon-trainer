import {Component, OnInit} from '@angular/core';
import {PokemonCatalogueService} from '../services/pokemon-catalogue.service';
import {Pokemon} from '../../../models/pokemon.model';
import {Router} from '@angular/router';
import {AppRoutes} from '../../../enums/app-routes.enum';

@Component({
  selector: 'app-pokemon-catalogue-container',
  templateUrl: './pokemon-catalogue.container.html',
  styleUrls: ['./pokemon-catalogue.container.css']
})
export class PokemonCatalogueContainer implements OnInit{
  /**
   * In constructor we initialize a pokemonCatalogueService and
   * a router.
   */
  constructor(
    private readonly pokemonCatalogueService: PokemonCatalogueService,
    private readonly router: Router){
  }
  /**
   * This methods returns a list of the pokemons from the service.
   */
  get pokemon(): Pokemon[]{
    return this.pokemonCatalogueService.pokemon;
  }
  get isLast(): boolean{
    return this.pokemonCatalogueService.lastPage;
  }
  get isFirst(): boolean{
    return this.pokemonCatalogueService.firstPage;
  }

  public nextPage(): void{
    return this.pokemonCatalogueService.next();
  }
  public prevPage(): void{
    return this.pokemonCatalogueService.prev();
  }
  /**
   * on initialization of the page we call the function fetchPokemons
   * to populate the pokemon list of the srevice.
   */
  ngOnInit(): void{
    this.pokemonCatalogueService.fetchPokemons();
  }
  /**
   * When the user clicks on a pokemon, we navigate to the pokemon-
   * detail page for the clicked pokemon.
   */
  goToPokemonDetail(pokemonName: string): void{
    this.router.navigateByUrl(AppRoutes.POKEMON_DETAIL + pokemonName);
  }
}
