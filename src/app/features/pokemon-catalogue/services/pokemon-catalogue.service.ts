import { Injectable } from '@angular/core';
import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import {environment} from '../../../../environments/environment';
import {map, shareReplay} from 'rxjs/operators';
import {PokemonResponse} from '../../../models/pokemon-respons.model';
import {Pokemon} from '../../../models/pokemon.model';

const { pokeAPI } = environment;

@Injectable({
  providedIn: 'root'
})
export class PokemonCatalogueService {
  /**
   * Initialise variables
   */
  private offset = 0;
  private limit = 20;
  public count = 0;
  public lastPage = false;
  public firstPage = true;
  private readonly pokemonCache$;
  private _pokemon: Pokemon[] = [];
  public error: string = '';
  /**
   * In constructor we initialize a httpClient. We also get and caches
   * 1118 pokemons from the pokemonAPI.
   */
  constructor( private readonly http: HttpClient) {
    this.pokemonCache$ = this.http.get<PokemonResponse>(`${pokeAPI}?limit=1118`)
      .pipe(shareReplay(1));
  }
  get pokemon(): Pokemon[]{
    return this._pokemon.slice(
      this.offset,
      this.offset + this.limit);
  }

  public next(): void{
    const nextOffset = this.offset + this.limit;
    if (nextOffset <= this.count - 1){
      this.offset += this.limit;
    }
    this.updatePageStatus();
  }
  public prev(): void{
    if (this.offset !== 0){
      this.offset -= this.limit;
    }
    this.updatePageStatus();
  }
  updatePageStatus(): void{
    this.firstPage = this.offset === 0;
    this.lastPage = this.offset === this.count - this.limit;
  }
  /**
   * In this method we fetch all of the 1118 pokemons from the cache.
   * We map over all of the pokemons and extract the id and image, and
   * store the pokemons in the pokemonslist as elements of type pokemon
   * -interface.
   */
  fetchPokemons(): void {
    this.pokemonCache$
      .pipe(
        map((response: PokemonResponse ) => {
          return response.results.map(pokemon => ({
            ...pokemon,
            ...this.getIdAndImage(pokemon.url)
          }));
        })
      )
      .subscribe(
        (pokemon: Pokemon[]) => {
          this._pokemon = pokemon;
          this.count = pokemon.length;
        },
        (errorResponse: HttpErrorResponse) => {
          this.error = errorResponse.message;
        }
      );
  }
  /**
   * This method gets the id of the pokemon and fetches the
   * pokemons image from. it returns an object of type pokemon
   * with the values id and image.
   */
  private getIdAndImage(url: string): Pokemon {
    const id = url.split( '/' ).filter( Boolean ).pop();
    return {id, image: `https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/${ id }.png`};
  }
}
