import {Component, OnInit} from '@angular/core';
import {Trainer} from '../../../models/trainer.model';
import {PokemonDetailService} from '../../pokemon-detail/services/pokemon-detail.service';
import {ActivatedRoute, Router} from '@angular/router';
import {getStorage} from '../../../utils/storage.utils';
import {StorageKeys} from '../../../enums/storage-keys.enum';
import {Pokemon} from '../../../models/pokemon.model';
import {AppRoutes} from '../../../enums/app-routes.enum';

@Component({
  selector: 'app-trainer-container',
  templateUrl: './trainer.container.html',
  styleUrls: ['./trainer.container.css']
})
export class TrainerContainer implements OnInit{
  // Initializing variables
  private storage: Trainer = {name: '', pokemons: []};
  pokemons: Pokemon[] = [];
  trainer: string = '';

  /**
   * Constructor.
   * Get the content in localstorage and initializing a router.
   */
  constructor(private readonly router: Router){
    this.storage = getStorage(StorageKeys.TRAINER);
  }

  /***
   * On initialization, we set the variable pokemons to be the pokemon list from localstorage.
   */
  ngOnInit(): void{
    this.pokemons = this.storage.pokemons;
    this.trainer = this.storage.name;
  }
  /**
   * If the user clicks on the pokemon-card,
   * the user is redirected to the pokemon-detail page for the given pokemon
   */
  goToPokemonDetail(pokemonName: string): void{
    this.router.navigateByUrl(AppRoutes.POKEMON_DETAIL + pokemonName);
  }

}
