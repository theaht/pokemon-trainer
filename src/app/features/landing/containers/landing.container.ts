import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';
import {getStorage, setStorage} from '../../../utils/storage.utils';
import {StorageKeys} from '../../../enums/storage-keys.enum';
import {Trainer} from '../../../models/trainer.model';
import {Router} from '@angular/router';
import {AppRoutes} from '../../../enums/app-routes.enum';

@Component({
  selector: 'app-landing-container',
  templateUrl: './landing.container.html',
  styleUrls: ['./landing.container.css']
})
export class LandingContainer implements OnInit{
  /**
   * setting a formgroup to capture the trainerName from input
   */
  landingForm: FormGroup = new FormGroup({
    trainerName: new FormControl('')
  });
  /**
   * In constructor we initialize a router
   */
  constructor(private readonly router: Router) {
  }
  /**
   * on initialization of the page we get storage to check if
   * storage is empty. If it's not empty we navigate to the
   * pokemon catalogue page.
   */
  ngOnInit(): void {
    const existingTrainer = getStorage(StorageKeys.TRAINER);
    console.log(existingTrainer);
    if (existingTrainer !== null){
      this.router.navigateByUrl(AppRoutes.POKEMON);
    }
  }
  /**
   * When user clicks the play button, we set storage with
   * the user input as trainerName and navigate to the
   * pokemon catalogue page.
   */
  startPlaying(): void{
    const trainer: Trainer = {name: this.landingForm.value.trainerName, pokemons: []};
    setStorage(StorageKeys.TRAINER, trainer);
    this.router.navigateByUrl(AppRoutes.POKEMON);
  }
}
