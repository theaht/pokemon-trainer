import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {LandingContainer} from './features/landing/containers/landing.container';
import {PokemonCatalogueContainer} from './features/pokemon-catalogue/containers/pokemon-catalogue.container';
import {PokemonDetailContainer} from './features/pokemon-detail/containers/pokemon-detail.container';
import {TrainerContainer} from './features/trainer/containers/trainer.container';
import {SessionGuard} from './guards/session.guard';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: '/landing'
  },
  {
    path: 'landing',
    component: LandingContainer
  },
  {
    path: 'pokemon',
    component: PokemonCatalogueContainer,
    canActivate: [SessionGuard]
  },
  {
    path: 'pokemon/:name',
    component: PokemonDetailContainer,
    canActivate: [SessionGuard]
  },
  {
    path: 'trainer',
    component: TrainerContainer,
    canActivate: [SessionGuard]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
