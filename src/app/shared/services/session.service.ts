import { getStorage } from 'src/app/utils/storage.utils';
import {Injectable} from '@angular/core';
import {StorageKeys} from '../../enums/storage-keys.enum';

@Injectable({
  providedIn: 'root'
})
export class SessionService{
  active(): boolean{
    const trainer = getStorage(StorageKeys.TRAINER);
    return Boolean(trainer);
  }
}
