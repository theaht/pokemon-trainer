export interface Pokemon{
  id: string;
  name?: string;
  url?: string;
  image: string;
  weight?: number;
  height?: number;
  types?: PokemonType[];
  stats?: PokemonStat[];
  abilities?: PokemonAbility[];
  moves?: PokemonMove[];
  base_experience?: number;
}

export interface PokemonType {
  slot: number;
  type: PokemonTypeType;
}

export interface PokemonTypeType {
  name: string;
}

export interface PokemonStat {
  base_stat: number;
  stat: PokemonStatStat;
}
export interface PokemonStatStat {
  name: string;
}

export interface PokemonAbility {
  ability: PokemonAbilityAbility;
}

export interface PokemonAbilityAbility {
  name: string;
}

export interface PokemonMove {
  move: PokemonMoveMove;
}

export interface PokemonMoveMove {
  name: string;
}
