import {Pokemon} from './pokemon.model';

export interface Trainer{
  name: string;
  pokemons: Pokemon[];
}
