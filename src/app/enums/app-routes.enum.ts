export enum AppRoutes{
  LANDING = '/landing',
  POKEMON = '/pokemon',
  POKEMON_DETAIL = '/pokemon/',
  TRAINER = '/trainer'
}
