# Pokemon Trainer

Application made by Camilla Stokbro Felin and Thea Thodesen.

In this application users can view and collect pokemons. Trainer name and collected pokemons are stored in localstorage. 
Only the landing page is accessible when localstorage is empty. 

## Landing page
The landing page contains a text field where the user can enter their trainer name. 
If there is already a username in the localstore, the user will be redirected to the pokemon catalogue page.

## Pokemon catalogue page
This page displays a list of pokemons, it is showing 20 pokemons on each page. 
The user can click on each pokemon card to navigate to the pokemon detail page. 
If the user clicks on Ash (the trainer icon) in the navbar, the user will navigate to the trainer page. 

## Pokemon detail page
This page contains details about the given pokemon. 
The user can click the collect button to add the pokemon to their list of collected pokemons.
The user can click on Ash (the trainer icon) in the navbar, to navigate to the trainer page, 
and the pokemon logo in the navbar to navigate to the pokemon catalogue page. 

## Trainer page

The trainer page contains a list of all pokemons the user has collected. 
The user can click on the pokemon logo in the navbar, to navigate to the pokemon catalogue page. 

## Angular 

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 11.2.2.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.
